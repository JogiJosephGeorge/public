class ConfigData:
    
    ConnectionString = None
    DbName = None
    CollectionSubject = 'subjects'
    CollectionTopic = 'topics'
    CollectionMember = 'members'
    
    def __init__(self):
        f = open("Config.txt", "r")
        self.ConnectionString = f.readline().replace("\n", "")
        self.DbName = f.readline().replace("\n", "")
