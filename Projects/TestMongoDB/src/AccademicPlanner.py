from Subject import Subject
from Topic import Topic
from Member import Member
from MongoDbMan import MongoDbMan
from ConfigData import ConfigData

class AccademicPlanner:
    configData = ConfigData()
    mongoDbMan = MongoDbMan()

    def __init__(self):
        self.mongoDbMan.Open(self.configData.ConnectionString)
        self.mongoDbMan.OpenDataBase(self.configData.DbName)

    def GetDbSummary(self):
        try:
            names = self.mongoDbMan.mydb.list_collection_names()
            msg = []
            for name in names:
                self.mongoDbMan.OpenCollection(name)
                msg.append({ 'DbName' : name, 'DocCount' : self.mongoDbMan.GetCount() })
            return msg
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def GetAllDocuments(self, collectionName):
        try:
            self.mongoDbMan.OpenCollection(collectionName)
            return self.mongoDbMan.Select()
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def ClearDb(self, jsonData):
        try:
            collectionName = jsonData['CollectionName']
            self.mongoDbMan.ClearCollection(collectionName);
            return "The table {0} has been removed.".format(collectionName)
        except Exception as inst:
            return "Exception occured : " + str(inst)

#------------Subjects-----------------------------------------

    def AddSubjectToDb(self, jsObj):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionSubject)
            subject = Subject(jsObj)
            return self.mongoDbMan.InsertOne(subject)
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def GetAllSubjects(self):
        return self.GetAllDocuments(self.configData.CollectionSubject)

    def GetSubject(self, subId):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionSubject)
            subject = Subject()
            subject.SubId = int(subId)
            for x in self.mongoDbMan.Select(subject):
                return vars(Subject(x))
            return "Error"
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def GetSubjectByCourse(self, course):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionSubject)
            subject = Subject()
            subject.Course = course
            subjects = []
            for x in self.mongoDbMan.Select(subject):
                subjects.append(vars(Subject(x)))
            result = {}
            result['data'] = subjects
            return result
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def UpdateSubject(self, jsObj):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionSubject)
            subject = Subject(jsObj)
            query = { "SubId" : subject.SubId }
            subject.SubId = None
            return self.mongoDbMan.UpdateOne(query, subject)
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def DeleteSubject(self, subId = 0):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionSubject)
            subject = Subject()
            subject.SubId = int(subId)
            deletedCnt = self.mongoDbMan.DeleteOne(subject)
            return "{0} subject(s) deleted.".format(deletedCnt)
        except Exception as inst:
            return "Exception occured : " + str(inst)

#------------Topics-------------------------------------------

    def AddTopicToDb(self, jsObj):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionTopic)
            topic = Topic(jsObj)
            return self.mongoDbMan.InsertOne(topic)
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def GetAllTopics(self):
        return self.GetAllDocuments(self.configData.CollectionTopic)

    def GetTopic(self, topicId):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionTopic)
            topic = Topic()
            topic.TopicId = int(topicId)
            for x in self.mongoDbMan.Select(topic):
                return vars(Subject(x))
            return "Error"
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def UpdateTopic(self, jsObj):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionTopic)
            topic = Topic(jsObj)
            query = { "TopicId" : topic.TopicId }
            topic.TopicId = None
            return self.mongoDbMan.UpdateOne(query, topic)
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def DeleteTopic(self, topicId = 0):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionTopic)
            topic = Topic()
            topic.TopicId = int(topicId)
            deletedCnt = self.mongoDbMan.DeleteOne(topic)
            return "{0} topic(s) deleted.".format(deletedCnt)
        except Exception as inst:
            return "Exception occured : " + str(inst)

#------------Members-------------------------------------------

    def AddMemberToDb(self, jsObj):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionMember)
            member = Member(jsObj)
            return self.mongoDbMan.InsertOne(member)
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def GetAllMembers(self):
        return self.GetAllDocuments(self.configData.CollectionMember)

    def GetMember(self, memberId):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionMember)
            member = Member()
            member.MemberId = int(memberId)
            for x in self.mongoDbMan.Select(member):
                return vars(Subject(x))
            return "Error"
        except Exception as inst:
            return "Exception occured : " + str(inst)

    def DeleteMember(self, memberId = 0):
        try:
            self.mongoDbMan.OpenCollection(self.configData.CollectionMember)
            member = Member()
            member.MemberId = int(memberId)
            deletedCnt = self.mongoDbMan.DeleteOne(member)
            return "{0} member(s) deleted.".format(deletedCnt)
        except Exception as inst:
            return "Exception occured : " + str(inst)
