class Subject:
    SubId = None
    Course = None
    Medium = None
    Category = None
    SubjectDesc = None
    Code = None
    TotalHours = None
    TotalTopics = None
    ExamDuration = None

    def __init__(self, myDict = None):
        if myDict != None:
            self.SubId = myDict.get('SubId')
            self.Course = myDict.get('Course')
            self.Medium = myDict.get('Medium')
            self.Category = myDict.get('Category')
            self.SubjectDesc = myDict.get('SubjectDesc')
            self.Code = myDict.get('Code')
            self.TotalHours = myDict.get('TotalHours')
            self.TotalTopics = myDict.get('TotalTopics')
            self.ExamDuration = myDict.get('ExamDuration')
