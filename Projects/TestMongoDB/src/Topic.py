class Topic:
    TopicId = None
    SubjectId = None
    Course = None
    Subject = None
    TopicDes = None
    Content = None
    THour = None

    def __init__(self, myDict = None):
        if myDict != None:
            self.TopicId = myDict.get('TopicId')
            self.SubjectId = myDict.get('SubjectId')
            self.Course = myDict.get('Course')
            self.Subject = myDict.get('Subject')
            self.TopicDes = myDict.get('TopicDes')
            self.Content = myDict.get('Content')
            self.THour = myDict.get('THour')
