from flask import Flask, abort, request, jsonify
import sys
import os
import json
from AccademicPlanner import AccademicPlanner
from flask.templating import render_template
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

accademicPlanner = AccademicPlanner()

#------------Common-------------------------------------------

@app.route('/')
def Index():
    return "Welcome"

@app.route('/GetDbSummary')
def GetDbSummary():
    records = accademicPlanner.GetDbSummary()
    colHeaders = ['DbName', 'DocCount']
    return render_template('record.html', records=records, colnames=colHeaders)

@app.route('/restart')
def restart():
    python = sys.executable
    os.execl(python, python, *sys.argv)

@app.route('/ClearDb', methods=['DELETE'])
def ClearDb():
    return ProcessJsonRequest(accademicPlanner.ClearDb)

#------------Subjects-----------------------------------------

@app.route('/AddSubject', methods=['POST'])
def AddSubject():
    return ProcessJsonRequest(accademicPlanner.AddSubjectToDb)

@app.route('/GetAllSubjects')
def GetAllSubjects():
    return jsonify(accademicPlanner.GetAllSubjects())

@app.route('/GetSubject/<subId>')
def GetSubject(subId = 0):
    return JsonResponse(accademicPlanner.GetSubject(subId))

@app.route('/GetSubjectByCourse/<course>')
def GetSubjectByCourse(course = None):
    return JsonResponse(accademicPlanner.GetSubjectByCourse(course))

@app.route('/UpdateSubject', methods=['POST'])
def UpdateSubject():
    return ProcessJsonRequest(accademicPlanner.UpdateSubject)

@app.route('/DeleteSubject/<subId>')
def DeleteSubject(subId = 0):
    return accademicPlanner.DeleteSubject(subId)

#------------Topics-------------------------------------------

@app.route('/AddTopic', methods=['POST'])
def AddTopic():       
    return ProcessJsonRequest(accademicPlanner.AddTopicToDb)

@app.route('/GetAllTopics')
def GetAllTopics():
    return jsonify(accademicPlanner.GetAllTopics())

@app.route('/GetTopic/<topicId>')
def GetTopic(topicId = 0):
    return JsonResponse(accademicPlanner.GetTopic(topicId))

@app.route('/UpdateTopic', methods=['POST'])
def UpdateTopic():
    return ProcessJsonRequest(accademicPlanner.UpdateTopic)

@app.route('/DeleteTopic/<topicId>')
def DeleteTopic(topicId = 0):
    return accademicPlanner.DeleteTopic(topicId)

#------------Members------------------------------------------

@app.route('/AddMember', methods=['POST'])
def AddMember():
    return ProcessJsonRequest(accademicPlanner.AddMemberToDb)

@app.route('/GetAllMembers')
def GetAllMembers():
    return jsonify(accademicPlanner.GetAllMembers())

@app.route('/GetMember/<memberId>')
def GetMember(memberId = 0):
    return JsonResponse(accademicPlanner.GetMember(memberId))

@app.route('/DeleteMember/<memberId>')
def DeleteMember(memberId = 0):
    return accademicPlanner.DeleteMember(memberId)

#------------Private Methods----------------------------------

def JsonResponse(retObj):
    if type(retObj) is dict:
        return jsonify(retObj)
    elif type(retObj) is str:
        return retObj
    return "Error."

def ProcessJsonRequest(fun1):
    msg = ''
    try:
        if not request.json:
            abort (400)
        data = request.data;
        jsonData = json.loads(data)
        msg = fun1(jsonData)
    except Exception as inst:
        msg = "Exception occured in AddSubject : " + str(inst)
    print (msg)
    return msg

def HtmlTable(records, tempObject):
    colHeaders = [attr for attr in dir(tempObject)
          if not callable(getattr(tempObject, attr))
          and not attr.startswith('_')]
    return render_template('record.html', records=records, colnames=colHeaders)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=81, debug=True)
