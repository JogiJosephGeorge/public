import pymongo

class MongoDbMan:

    myclient = None
    mydb = None
    mycol = None

    def Open(self, conStr):
        self.myclient = pymongo.MongoClient(conStr)

    def OpenDataBase(self, dbName):
        self.mydb = None
        self.mycol = None
        if self.myclient != None:
            self.mydb = self.myclient[dbName]

    def OpenCollection(self, collectionName):
        self.mycol = None
        if self.mydb != None:
            self.mycol = self.mydb[collectionName]

    def InsertOne(self, myDict):
        if self.mycol == None:
            return None
        return self.mycol.insert_one(myDict)

    def InsertMany(self, myList):
        if self.mycol == None:
            return None
        return self.mycol.insert_many(myList)

    def Select(self, selObject = None, fields = None):
        if self.mycol == None:
            return None
        myDict = None
        if fields != None:
            myDict = dict();
            myDict['_id'] = 0
            for field in fields:
                myDict[field] = 1
        if selObject != None:
            query = self.GetDict(selObject)
        else:
            query = {}
        result1 = []
        for x in self.mycol.find(query, myDict):
            result1.append(self.RemoveEmptyFields(x))
        return result1

    def GetCount(self):
        return self.mycol.find().count()

    def UpdateOne(self, query, newObject):
        if self.mycol == None:
            return None

        newQuery = self.GetDict(newObject)
        newvalues = { "$set": newQuery }
        res = self.mycol.update_one(query, newvalues)
        return res.modified_count

    def DeleteOne(self, delObject):
        query = self.GetDict(delObject)
        res = self.mycol.delete_one(query)
        return res.deleted_count

    def ClearCollection(self, collectionName):
        if self.mydb != None:
            res = self.mydb[collectionName].drop()
            print (res)

    def GetDict(self, object1):
        dict1 = vars(object1)
        return self.RemoveEmptyFields(dict1)

    def RemoveEmptyFields(self, dict1):
        return { k : v for k, v in dict1.items() if v is not None and k != '_id'}
