﻿using System.Collections.ObjectModel;
using System.Windows;
using WPFGUI.UserControls;
using WPFGUI.UserControls.NumericUpdownUnit;

namespace WpfApp1
{
   public class MainVM
   {
      public MainVM()
      {
         PatternFacade = new PatternFacade();
         PropertiesViewModels = new ObservableCollection<UserControlBaseViewmodel>();
      }

      public PatternFacade PatternFacade { get; set; }

      public ObservableCollection<UserControlBaseViewmodel> PropertiesViewModels { get; set; }
   }

   public class PatternFacade
   {
      public PatternFacade()
      {
      }

      public UserControlBaseViewmodel DistanceVM { get; set; }

      public bool ShowDistance { get; set; }

      public double Distance { get; set; }

      public double DistanceStepValue { get; set; }

      public double DistanceMin { get; set; }

      public double DistanceMax { get; set; }

      public string DistanceFormat { get; set; }

      public string DistanceUnit { get; set; }
   }

   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         //List<int> ii = new List<int>();
         //var aa = ii.ToList().DefaultIfEmpty(4).Max();

         InitializeComponent();

         //RoutedCommand newCmd = new RoutedCommand();
         //newCmd.InputGestures.Add(new KeyGesture(Key.F4, ModifierKeys.Alt));
         //CommandBindings.Add(new CommandBinding(newCmd, btnNew_Click));

         BindValue = 3;

         var mainVm = new MainVM();

         mainVm.PatternFacade.ShowDistance = true;
         mainVm.PatternFacade.DistanceStepValue = 2;
         mainVm.PatternFacade.Distance = 12;
         mainVm.PatternFacade.DistanceMin = 5;
         mainVm.PatternFacade.DistanceMax = 45;
         mainVm.PatternFacade.DistanceFormat = "N3";
         mainVm.PatternFacade.DistanceUnit = "micron";
         mainVm.PatternFacade.DistanceVM = new NumericUpdownUnitViewmodel(null)
         {
            Source = "GroupProperties",
            LabelContent = "Test Label 0",
            BindingValue = 25,
            UnitContent = "Meter",
            MinValue = 20,
            MaxValue = 50,
            FormatString = "N0",
            AllowTextInput = false,
            Type = ControlType.NUMERICUPDOWN,
         };

         mainVm.PropertiesViewModels.Add(new NumericUpdownUnitViewmodel(null)
         {
            Source = "GroupProperties",
            LabelContent = "Test Label 1",
            BindingValue = BindValue,
            UnitContent = "CenMeter",
            MinValue = 2,
            MaxValue = 5,
            FormatString = "N1",
            Type = ControlType.NUMERICUPDOWN,
         });
         mainVm.PropertiesViewModels.Add(new NumericUpdownUnitViewmodel(null)
         {
            Source = "GroupProperties",
            LabelContent = "Test Label 2",
            BindingValue = BindValue,
            UnitContent = "Milli",
            MinValue = 10,
            MaxValue = 25,
            StepValue = 3,
            FormatString = "N2",
            Type = ControlType.NUMERICUPDOWN,
         });

         DataContext = mainVm;
      }

      public int BindValue { get; set; }
      //void btnNew_Click(object sender, ExecutedRoutedEventArgs e)
      //{
      //   Debug.WriteLine("Clicked");
      //}
   }
}
