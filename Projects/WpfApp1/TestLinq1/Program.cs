﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLinq1
{
   class Person
   {
      public int Age { get; set; }
      public int Mark { get; set; }
   }

   class Employee : Person
   {
      public int Salary { get; set; }
   }

   class Program
   {
      static void Main(string[] args)
      {
         var p = new List<Person>
         {
            new Person { Age = 5, Mark = 0},
            new Employee { Age = 15, Salary = 200}
         };

         var p1 = p
            .Where(item => item.Age > 10)
            .Select(item => item as Employee)
            .Where(item => item.Salary > 100)
            .ToList();

      }
   }
}
