// TestCppCode.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <fstream>

class MyConstTestClsA
{
   int m_one;
public:
   int Method1()
   {
      return 101;
   }

   int Method1() const
   {
      // m_one = 23; // Error : Expression must be a modifiable lvalue
      return 102;
   }
};

int main()
{
   MyConstTestClsA a1;
   auto n1 = a1.Method1();

   const MyConstTestClsA a2;
   auto n2 = a2.Method1();

   int x;
   std::ifstream inFile;
   inFile.open("D:\\ZPos.txt");
   while (inFile >> x) {
   }
   return 0;
}

