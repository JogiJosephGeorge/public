from flask import Flask, request
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    uploadPath = './uploaded'
    try:
        os.stat(uploadPath)
    except:
        os.mkdir(uploadPath)
    if request.method == 'POST':
        file = request.files['file']
        print ('File Name : ' + file.filename)

        if file:
            fileName = secure_filename(file.filename)
            file.save(os.path.join(uploadPath, fileName))

    return '''
        <!doctype html>
        <form method=post enctype=multipart/form-data>
        <p><input type=file name=file>
        <input type=submit value=Upload>
        </form>
        '''

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
