from Src.UserInfo import UserInfo

import sys, os
from _datetime import datetime
sys.path.append(os.path.abspath(os.path.join('../../', 'TestMongoDB/Src')))
from ConfigData import ConfigData
from MongoDbMan import MongoDbMan

class TaskManager:
    mongoDbMan = MongoDbMan()

    def __init__(self):
        configData = ConfigData()
        self.mongoDbMan.Open(configData.ConnectionString)
        self.mongoDbMan.OpenDataBase(configData.DbName)

        self.AddUser('jogi', '123')
        self.AddUser('jose', '567')

    def AddUser(self, userName, pwd):
        self.mongoDbMan.OpenCollection('TaskUserInfo')
        userInfo = UserInfo();
        userInfo.UserName = userName
        userInfo.Password = pwd
        userInfo.UId = datetime.now().__str__()
        # Make sure that (username, pwd) is unique
        # Make sure that Uid is unique
        self.mongoDbMan.InsertOne(vars(userInfo))

    def CheckCredentials(self, userName, pwd):
        self.mongoDbMan.OpenCollection('TaskUserInfo')
        userInfo = UserInfo();
        userInfo.UserName = userName
        userInfo.Password = pwd
        for x in self.mongoDbMan.Select(userInfo):
            return UserInfo(x).UId

    def GetUserName(self, userId):
        self.mongoDbMan.OpenCollection('TaskUserInfo')
        userInfo = UserInfo();
        userInfo.UId = userId
        for x in self.mongoDbMan.Select(userInfo):
            return UserInfo(x).UserName
