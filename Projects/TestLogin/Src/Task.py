class Task:
    TaskId = None
    TaskName = None
    SubTaskName = None
    Hours = None
    IsVerified = None
    Project = None

    def __init__(self, myDict = None):
        if myDict != None:
            self.TaskId = myDict.get('TaskId')
            self.TaskName = myDict.get('TaskName')
            self.SubTaskName = myDict.get('SubTaskName')
            self.Hours = myDict.get('Hours')
            self.IsVerified = myDict.get('IsVerified')
            self.Project = myDict.get('Project')
