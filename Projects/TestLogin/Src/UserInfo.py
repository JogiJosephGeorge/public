class UserInfo:
    UId = None
    UserName = None
    Password = None

    def __init__(self, myDict = None):
        if myDict != None:
            self.UId = myDict.get('UId')
            self.UserName = myDict.get('UserName')
            self.Password = myDict.get('Password')
