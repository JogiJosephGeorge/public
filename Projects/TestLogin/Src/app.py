from flask import Flask, render_template, url_for, redirect
from Src.Login import LoginHandler
from Src.TaskManager import TaskManager

app = Flask(__name__)

taskManager = TaskManager()
loginHandler = LoginHandler()

@app.route("/")
def Home():
    user = loginHandler.IsValidSession(taskManager.GetUserName)
    if user:
        return render_template('TaskInputPage.html', user=user)
    return redirect(url_for('login'))

@app.route("/login", methods=["GET", "POST"])
def login():
    return loginHandler.Login('Home', taskManager.CheckCredentials)

if __name__ == "__main__":
    #app.env = ''
    #app.debug = True
    app.run(host='0.0.0.0', port='182')
