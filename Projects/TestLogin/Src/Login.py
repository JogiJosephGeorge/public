from flask import request, render_template, url_for, redirect

class LoginHandler:

    UserNameFieldName = 'username'
    PwdFieldName = 'password'
    SessionIdCookie = 'YourSessionCookie'
    LoginPageName = 'Login.html'

    def Login(self, urlOnSuccess, checkPwd):
        msg = None
        try:
            if request.method == "POST":
                user_name = request.form[self.UserNameFieldName]
                password = request.form[self.PwdFieldName]
                sessionId = checkPwd(user_name, password)
                if sessionId:
                    response = redirect(url_for(urlOnSuccess))
                    response.set_cookie(self.SessionIdCookie, sessionId)
                    return response
                else:
                    msg = "Invalid credentials"
        except Exception as inst:
            msg = "Exception occured : " + str(inst)
        return render_template(self.LoginPageName, error = msg)

    def IsValidSession(self, getUserName):
        sessionId = request.cookies.get(self.SessionIdCookie)
        if sessionId:
            user = getUserName(sessionId)
            if user:
                return user
