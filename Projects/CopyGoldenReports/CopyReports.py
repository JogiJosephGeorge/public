import os
import glob
from shutil import copyfile
import sys

if len(sys.argv) != 3:
    print ("  *** Invalid Arguments are given ***  ")

#srcDir = 'D:/QuEST/CI/SourceCode3'
#testName = 'CDA/Mmi/Intel/CDA-163_Bga3D_with_reflow'

srcDir = sys.argv[1]
testName = sys.argv[2]
 
goldenPath = srcDir + '/handler/tests/' + testName + '/GoldenReports'
resultsPath = srcDir + '/handler/tests/' + testName + '~/_results'

try:
    for path, subdirs, files in os.walk(goldenPath):
        relPath = os.path.relpath(path, goldenPath)
        destinationPath = os.path.join(goldenPath, relPath)
        for fileName in files:
            resultFile = os.path.join(resultsPath, relPath, fileName)
            destinFile = os.path.join(destinationPath, fileName)
            destinFile = destinFile.replace("/", "\\")
            if os.path.exists(resultFile):
                resultFile = resultFile.replace("/", "\\")
                print ('copy ' + resultFile + ' ' + destinFile)
                os.remove(destinFile)
                copyfile(resultFile, destinFile)
                continue
            resultFile = resultFile.replace("'", "?")
            globResult = glob.glob(resultFile)
            if len(globResult) == 1:
                resultFile = globResult[0]
            if os.path.exists(resultFile):
                resultFile = resultFile.replace("/", "\\")
                print ('copy ' + resultFile + ' ' + destinFile)
                os.remove(destinFile)
                copyfile(resultFile, destinFile)
                continue
            print ("  *** ERROR ***  ")
except Exception as ex:
    print (str(ex))