﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XmlSerializerTest
{
   public class MyClass
   {
      public int MyProperty1 { get; set; }

      [XmlElement("B")]
      [JsonProperty("B1")]
      public int MyProperty2 { get; set; }
   }

   class Program
   {
      static void Main(string[] args)
      {
         var ls = new List<int>() { 23, 45, 67, 78 };
         var cnt = ls.RemoveAll(item => item > 5);

         var myObj = new MyClass();

         using (var streamWriter = new StreamWriter("Test.xml"))
         {
            var xmlSerializer = new XmlSerializer(typeof(MyClass));
            xmlSerializer.Serialize(streamWriter.BaseStream, myObj);
         }

         var strContent = JsonConvert.SerializeObject(myObj, Formatting.Indented);
         File.WriteAllText("Test.json", strContent);
      }
   }
}
