from flask import Flask, render_template, request
from flask.helpers import url_for

app = Flask(__name__)

@app.route("/")
@app.route("/index", methods=['GET', 'POST'])
def index():
    data = request.form
    try:
        filename = url_for('static', filename = 'images/' + data['studentid'] + '.JPG')
    except:
        filename = ''
    data.filename = filename
    return render_template('index.html', result = data)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
