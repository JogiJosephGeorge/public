﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlReaderCS
{
   class Program
   {
      static void Main(string[] args)
      {
         //Create a connection calling the App.config
         string conn = ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
         //The query to use
         string query = "SELECT * FROM SalesLT.Customer";
         SqlConnection connection = new SqlConnection(conn);
         //Create a Data Adapter
         SqlDataAdapter dadapter = new SqlDataAdapter(query, connection);
         //Create the dataset
         DataSet ds = new DataSet();
         //Open the connection
         connection.Open();
         //Fill the Data Adapter
         dadapter.Fill(ds, "SalesLT.Customer");
         connection.Close();
         //Bind the datagridview with the data set
      }
   }
}
